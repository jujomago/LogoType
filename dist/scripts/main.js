'use strict';

$(document).ready(function () {

    // Control for the switch buttons ( register , home)
    var $toogleSw = $('#toggle-user-transport');

    $('.btn-switch').on('click', function (e) {
        $('.btn-switch').removeClass('active');
        if (e.currentTarget.id === 'switch-user' && $toogleSw.is(':checked')) {
            $(e.currentTarget).addClass('active');
            $toogleSw.prop('checked', false);
        }
        if (e.currentTarget.id === 'switch-transport' && !$toogleSw.is(':checked')) {
            $(e.currentTarget).addClass('active');
            $toogleSw.prop('checked', true);
        }
    });

    $toogleSw.on('click', function (e) {
        $('.btn-switch').removeClass('active');

        if ($(e.currentTarget).prop('checked')) {
            $('#switch-transport').addClass('active');
        } else {
            $('#switch-user').addClass('active');
        }
    });

    // Control for navbar buttons and the sidemenus
    $('#toggle-side-right,#toggle-side-left').on('change', function (e) {
        if (e.currentTarget.id === 'toggle-side-left') {
            $('#toggle-side-right').prop('checked', false);
        }
        if (e.currentTarget.id === 'toggle-side-right') {
            $('#toggle-side-left').prop('checked', false);
        }
    });

    /*$('.layer-overlay').on('click',()=>{
        $('.off-canvas > input').prop('checked',false);
    });*/
});
//# sourceMappingURL=main.js.map
